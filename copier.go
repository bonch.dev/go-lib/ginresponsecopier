package ginresponsecopier

import (
	"bytes"

	"github.com/gin-gonic/gin"
)

// GinResponseCopier is a struct, that copies gin.Response to self
type GinResponseCopier struct {
	gin.ResponseWriter
	Body *bytes.Buffer
}

// New return new instance of GinResponseCopier
func New(responseWriter gin.ResponseWriter) *GinResponseCopier {
	return &GinResponseCopier{
		Body:           bytes.NewBufferString(""),
		ResponseWriter: responseWriter,
	}
}

// Write tees data to local buffer and to gin body
func (w *GinResponseCopier) Write(b []byte) (int, error) {
	w.Body.Write(b)

	return w.ResponseWriter.Write(b)
}

// Bytes return current buffer of GinResponseCopier
func (w *GinResponseCopier) Bytes() []byte {
	return w.Body.Bytes()
}
